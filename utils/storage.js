const setStorageSync = function(key, data) {
	try {
		uni.setStorageSync(key, data);
		return true;
	} catch (e) {
		// error
		return false;
	}
}
const getStorageSync = function(key,defaultBack) {
	try {
		const value = uni.getStorageSync(key);
		if(value==''){
			return defaultBack;
		}else{
			return value;
		}
		
	} catch (e) {
		// error
		return defaultBack;
	}
}
const removeStorageSync = function(key) {
	try {
		uni.removeStorageSync(key);
		return true;
	} catch (e) {
		// error
		return false;
	}
}
const clearStorageSync = function() {
	try {
		uni.clearStorageSync();
		return true;
	} catch (e) {
		// error
		return false;
	}
}
export default {
	setStorageSync,
	getStorageSync,
	removeStorageSync,
	clearStorageSync
}
