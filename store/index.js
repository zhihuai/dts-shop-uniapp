import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const store = new Vuex.Store({
	state: {
		hasLogin: false,
		user: null,
		code:'',
	},
	mutations: {
		login(state, user) {
			state.hasLogin = true;
			state.user = user;
		},
		logout(state) {
			state.hasLogin = false;
			state.user = user;
		},
		setCode(state,code){
			state.code = code;
		}
	}
});

export default store
