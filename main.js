import Vue from 'vue'
import App from './App'

Vue.config.productionTip = false

import store from './store'
Vue.prototype.$store = store;

import storage from 'utils/storage.js'
Vue.prototype.$storage = storage;

import util from 'utils/util.js'
Vue.prototype.$util = util;

import userJs from 'utils/user.js'
Vue.prototype.$userJs = userJs;

import api from 'utils/config/api.js'
Vue.prototype.$api = api;

import check from 'utils/check.js'
Vue.prototype.$check = check;

App.mpType = 'app'

const app = new Vue({
    ...App,store
})
app.$mount()
