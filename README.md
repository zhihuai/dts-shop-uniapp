# dts-shop-uniapp

#### 介绍
基于开源[dts-shop](https://gitee.com/qiguliuxing/dts-shop)的uniapp

#### 时间节点
2020-05-26 整体小程序迁移完成，未进行系统测试测试

#### 软件架构
没啥可说的

#### 安装教程
hbuilderX，导入项目。
怎么玩，自己去[uniapp](https://uniapp.dcloud.io/)官网查询

#### 使用说明
自己点点看看

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
